import React , {useState, useEffect} from 'react';
import styled from '@emotion/styled';
import Frase from './component/Frase'

const Contenedor = styled.div`
  display: flex;
  align-items: center;
  padding-top: 5rem;
  flex-direction: column;
` ;

const Boton = styled.button`
  background: -webkit-linear-gradient( #007d35 0% , #0f574e 100%);
  background-size: 300px;
  font-family:  Arial, Helvetica, sans-serif;
  color: #fff;
  margin-top: 3rem;
  padding: 1rem 3rem;
  font-size: 2rem;
  border: 2px solid black;
`;

function App() {

  const [frase , guardarFrase] = useState({})
  
  const consultaApi = async ()=>{
    const api = await fetch('https://breaking-bad-quotes.herokuapp.com/v1/quotes')
    const resultado = await api.json()
    guardarFrase(resultado[0])    
  }

  useEffect(()=>{
    consultaApi()
  },[])

  return (
    <Contenedor>
      {frase? <Frase frase = {frase} />:null}
      <Boton
        onClick = {consultaApi}
      > Obten tu frace chingona </Boton>
    </Contenedor>
  );
}

export default App;
