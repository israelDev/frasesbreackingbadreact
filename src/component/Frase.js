import React, { Fragment } from 'react';
import styled from '@emotion/styled';
const Frase = ({ frase }) => {

    const ContendorFrase = styled.div`
    padding : 3rm;
    border-radius : .5rem;
    max-width:1000px;
    background-color : #fff;
    @media (min-width: 992px) {
        margin-top:10rem;    
    }
    h1{
        font-family: Arial, Helvetica, sans-serif;
        text-align:center;
        position: relative;
        padding: 25px 25px 25px 25px;

        &::before{
            content: open-quote;
            font-size: 8rem;
            color: black;
            position: absolute;
            left: -1rem;
            top : -2rem;

        }
    }
    p{
        font-family : Verdana, Geneva, Tahoma, sans-serif;
        font-size: 1.4rem;
        text-align:right;
        font-weight: bold;
        color: #666;
        margin-top:2rem;


    }
` ;

    if (Object.keys(frase).length === 0) return null;


    return (
        <Fragment>
            <ContendorFrase>
                <h1>{frase.quote} </h1>
                <p> {frase.author} </p>
            </ContendorFrase>
        </Fragment>

    );
}

export default Frase;